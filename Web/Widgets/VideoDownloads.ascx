﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoDownloads.ascx.cs" Inherits="SitefinityWebApp.Widgets.VideoDownloads" %>

<asp:Repeater ID="vidRepeater" runat="server" 
    onitemcommand="vidRepeater_ItemCommand">
    <HeaderTemplate>
        <ul class="vidList white-papers">
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <asp:Image ID="Image1" runat="server" ImageUrl='<%#  DataBinder.Eval(Container.DataItem, "ThumbnailUrl") %>' /><br />
            <asp:Label ID="label1" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Title") %>' /><br />
            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DownloadUrl") %>' Text="Download"
                OnClientClick='<%# String.Format("TrackBroadcastAssets({0}{1}{2});", "\"",DataBinder.Eval(Container.DataItem, "Title"), "\"") %>'></asp:LinkButton>
            <asp:Label ID="lblSize" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FileSize") %>' />
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
