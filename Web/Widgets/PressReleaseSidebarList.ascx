﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PressReleaseSidebarList.ascx.cs" Inherits="SitefinityWebApp.Widgets.PressReleaseSidebarList" %>

<asp:Repeater ID="PressReleaseSidebar" runat="server">
    <HeaderTemplate>
        <ul class="press-sidebar-list">
    </HeaderTemplate>
     <ItemTemplate>
        <li>
            <a href='<%# Eval("StringUrl") %>'> <%# Eval("Title").ToString().Length <= 50 ? Eval("Title").ToString() : Eval("Title").ToString().Substring(0, 50) + "..."%> </a><br />
        </li>
    </ItemTemplate>
    <FooterTemplate>
        <li>
            <a href="/newsroom/press-releases">View More</a>
        </li>
        </ul>
    </FooterTemplate>
</asp:Repeater>