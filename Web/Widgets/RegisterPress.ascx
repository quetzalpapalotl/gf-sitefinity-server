﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI.Fields" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Web.UI" Assembly="Telerik.Sitefinity" %>
 <%@ Register TagPrefix="sf" Namespace="Telerik.Sitefinity.Modules.GenericContent.Web.UI" Assembly="Telerik.Sitefinity" %>
<%@ Register TagPrefix="sfvalidation" Namespace="Telerik.Sitefinity.Web.UI.Validation.Definitions" Assembly="Telerik.Sitefinity"%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

  
<fieldset class="sfregisterFormWrp">
<sf:FormManager ID="formManager" runat="server" />
    <asp:Panel ID="formContainer" runat="server" DefaultButton="registerButton">
        <ol class="sfregisterFieldsList pressRegisterLeft">
            <sf:TextField ID="firstName" runat="server" DataFieldName="FirstName" DataItemType="Telerik.Sitefinity.Security.Model.SitefinityProfile" DisplayMode="Write" Title="<%$ Resources:Labels, FirstName %>" CssClass="sfregisterField sfregisterFirstName" WrapperTag="li" TabIndex="10">
                <ValidatorDefinition MessageCssClass="sfError" Required="true"/>
            </sf:TextField>
            <asp:RequiredFieldValidator ID="reqFirst" runat="server" ErrorMessage="First name is required" 
            ControlToValidate="firstName" CssClass="sfError" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
             <sf:TextField ID="lastName" runat="server" DataFieldName="LastName" DataItemType="Telerik.Sitefinity.Security.Model.SitefinityProfile" DisplayMode="Write" Title="<%$ Resources:Labels, LastName %>" CssClass="sfregisterField sfregisterLastName" WrapperTag="li" TabIndex="11">
                <ValidatorDefinition MessageCssClass="sfError" Required="true"/>                                 
            </sf:TextField>
            <asp:RequiredFieldValidator ID="reqLast" runat="server" ErrorMessage="Last name is required" 
            ControlToValidate="lastName" CssClass="sfError" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            <sf:TextField ID="email" runat="server" DataFieldName="Email" DisplayMode="Write" Title="<%$ Resources:Labels, Email %>" CssClass="sfregisterField sfregisterEmail" WrapperTag="li" TabIndex="12">
                <ValidatorDefinition MessageCssClass="sfError" Required="true" ExpectedFormat="EmailAddress"/>
            </sf:TextField>
            <asp:RegularExpressionValidator ID="refEmail" runat="server" ErrorMessage="Invalid email format"
                ControlToValidate="email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ForeColor="Red" CssClass="sfError" EnableClientScript="true" Display="Dynamic"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="reqEmail" runat="server" ErrorMessage="Email is required" 
            ControlToValidate="email" CssClass="sfError" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
             <sf:TextField ID="companyName" runat="server" DataFieldName="CompanyName" DataItemType="Telerik.Sitefinity.Security.Model.SitefinityProfile" DisplayMode="Write" Title="<%$ Resources:Labels, CompanyName %>" CssClass="sfregisterField sfregisterCompanyName" WrapperTag="li" TabIndex="13">
                <ValidatorDefinition MessageCssClass="sfError" Required="true"/>                                 
            </sf:TextField>
           <asp:RequiredFieldValidator ID="reqCompany" runat="server" ErrorMessage="Company name is required" 
            ControlToValidate="companyName" CssClass="sfError" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        </ol>
        <ol class="pressRegisterRight">
              <sf:TextField ID="userName" runat="server" DataFieldName="UserName" DisplayMode="Write" Title="<%$ Resources:Labels, UserName %>" CssClass="sfregisterField sfregisterUserName" WrapperTag="li" TabIndex="15">
                <ValidatorDefinition MessageCssClass="sfError" Required="true"/>
            </sf:TextField>
            <asp:RequiredFieldValidator ID="reqUsername" runat="server" ErrorMessage="Username is required" 
            ControlToValidate="userName" CssClass="sfError" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            <sf:TextField ID="password" runat="server" DisplayMode="Write" Title="<%$ Resources:Labels, Password %>" IsPasswordMode="true" CssClass="sfregisterField sfregisterPassword" WrapperTag="li" TabIndex="16">
                <ValidatorDefinition MessageCssClass="sfError" Required="true"/>
            </sf:TextField>
            <asp:RequiredFieldValidator ID="reqPassword" runat="server" ErrorMessage="Password is required" ControlToValidate="password" CssClass="sfError" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            <sf:TextField ID="reTypePassword" runat="server" DisplayMode="Write" Title="<%$ Resources:UserProfilesResources, ReTypePassword %>" IsPasswordMode="true" CssClass="sfregisterField sfregisterConfirmPassword" WrapperTag="li" TabIndex="17">
                <ValidatorDefinition MessageCssClass="sfError">
                    <ComparingValidatorDefinitions>
                        <sfvalidation:ComparingValidatorDefinition ControlToCompare="password"
                            Operator="Equal" ValidationViolationMessage="<%$ Resources:ErrorMessages, CreateUserWizardDefaultConfirmPasswordCompareErrorMessage %>"/>
                    </ComparingValidatorDefinitions>
                </ValidatorDefinition>
            </sf:TextField>
            <asp:RequiredFieldValidator ID="reqCompareField" runat="server" ErrorMessage="Please confirm your password" ControlToValidate="reTypePassword" CssClass="sfError" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="cmpPasswords" runat="server" ErrorMessage="Passwords must match" 
                    ForeColor="Red" ControlToCompare="password" ControlToValidate="reTypePassword" CssClass="sfError" Display="Dynamic">
                </asp:CompareValidator>
        </ol> 
        <div class="sfregisterLnkWrp pressRegisterButton">
            <asp:Button runat="server" ID="registerButton" Text="<%$ Resources:UserProfilesResources, Register %>" CssClass="sfregisterSaveLnk"/>
        </div>
        <asp:Panel ID="errorsPanel" runat="server" CssClass="sfErrorPanel" Visible="false"/>
    </asp:Panel>
    <asp:Label runat="server" ID="successMessageLabel"></asp:Label>
    <asp:Panel ID="configurationErrorsPanel" runat="server" CssClass="sfErrorSummary" Visible="false" >
        <div runat="server" id="smtpSettingsErrorWrapper" Visible="false">
            <asp:Label runat="server" id="smtpConfigurationErrorTitle" Text="<%$ Resources:ErrorMessages, CannotSendEmails %>"/>
            <asp:Label runat="server" id="smtpConfigurationError"></asp:Label>
        </div>
    </asp:Panel>
  
</fieldset>