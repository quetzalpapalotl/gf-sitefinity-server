Type.registerNamespace("SitefinityWebApp.WidgetDesigners.SubpageSideNavigation");

SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner = function (element) {
    /* Initialize PageID fields */
    this._pageSelectorPageID = null;
    this._selectorTagPageID = null;
    this._PageIDDialog = null;
 
    this._showPageSelectorPageIDDelegate = null;
    this._pageSelectedPageIDDelegate = null;
    
    /* Calls the base constructor */
    SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner.initializeBase(this, [element]);
}

SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner.prototype = {
    /* --------------------------------- set up and tear down --------------------------------- */
    initialize: function () {
        /* Here you can attach to events or do other initialization */
        SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner.callBaseMethod(this, 'initialize');

        /* Initialize PageID */
        this._showPageSelectorPageIDDelegate = Function.createDelegate(this, this._showPageSelectorPageIDHandler);
        $addHandler(this.get_pageSelectButtonPageID(), "click", this._showPageSelectorPageIDDelegate);

        this._pageSelectedPageIDDelegate = Function.createDelegate(this, this._pageSelectedPageIDHandler);
        this.get_pageSelectorPageID().add_doneClientSelection(this._pageSelectedPageIDDelegate);

        if (this._selectorTagPageID) {
            this._PageIDDialog = jQuery(this._selectorTagPageID).dialog({
                autoOpen: false,
                modal: false,
                width: 395,
                closeOnEscape: true,
                resizable: false,
                draggable: false,
                zIndex: 5000
            });
        }
    },
    dispose: function () {
        /* this is the place to unbind/dispose the event handlers created in the initialize method */
        SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner.callBaseMethod(this, 'dispose');

        /* Dispose PageID */
        if (this._showPageSelectorPageIDDelegate) {
            $removeHandler(this.get_pageSelectButtonPageID(), "click", this._showPageSelectorPageIDDelegate);
            delete this._showPageSelectorPageIDDelegate;
        }

        if (this._pageSelectedPageIDDelegate) {
            this.get_pageSelectorPageID().remove_doneClientSelection(this._pageSelectedPageIDDelegate);
            delete this._pageSelectedPageIDDelegate;
        }
    },

    /* --------------------------------- public methods ---------------------------------- */

    findElement: function (id) {
        var result = jQuery(this.get_element()).find("#" + id).get(0);
        return result;
    },

    /* Called when the designer window gets opened and here is place to "bind" your designer to the control properties */
    refreshUI: function () {
        var controlData = this._propertyEditor.get_control().Settings; /* JavaScript clone of your control - all the control properties will be properties of the controlData too */

        /* RefreshUI PageID */
        if (controlData.PageID && controlData.PageID !== "00000000-0000-0000-0000-000000000000") {
            var pagesSelectorPageID = this.get_pageSelectorPageID().get_pageSelector();
            var selectedPageLabelPageID = this.get_selectedPageIDLabel();
            var selectedPageButtonPageID = this.get_pageSelectButtonPageID();
            pagesSelectorPageID.add_selectionApplied(function (o, args) {
                var selectedPage = pagesSelectorPageID.get_selectedItem();
                if (selectedPage) {
                    selectedPageLabelPageID.innerHTML = selectedPage.Title.Value;
                    jQuery(selectedPageLabelPageID).show();
                    selectedPageButtonPageID.innerHTML = '<span>Change</span>';
                }
            });
            pagesSelectorPageID.set_selectedItems([{ Id: controlData.PageID}]);
        }        
    },

    /* Called when the "Save" button is clicked. Here you can transfer the settings from the designer to the control */
    applyChanges: function () {
        var controlData = this._propertyEditor.get_control().Settings;

        /* ApplyChanges PageID */
    },

    /* --------------------------------- event handlers ---------------------------------- */

    /* --------------------------------- private methods --------------------------------- */

    /* PageID private methods */
    _showPageSelectorPageIDHandler: function (selectedItem) {
        var controlData = this._propertyEditor.get_control().Settings;
        var pagesSelector = this.get_pageSelectorPageID().get_pageSelector();
        if (controlData.PageID) {
            pagesSelector.set_selectedItems([{ Id: controlData.PageID }]);
        }
        this._PageIDDialog.dialog("open");
        jQuery("#designerLayoutRoot").hide();
        this._PageIDDialog.dialog().parent().css("min-width", "355px");
        dialogBase.resizeToContent();
    },

    _pageSelectedPageIDHandler: function (items) {
        var controlData = this._propertyEditor.get_control().Settings;
        var pagesSelector = this.get_pageSelectorPageID().get_pageSelector();
        this._PageIDDialog.dialog("close");
        jQuery("#designerLayoutRoot").show();
        dialogBase.resizeToContent();
        if (items == null) {
            return;
        }
        var selectedPage = pagesSelector.get_selectedItem();
        if (selectedPage) {
            this.get_selectedPageIDLabel().innerHTML = selectedPage.Title.Value;
            jQuery(this.get_selectedPageIDLabel()).show();
            this.get_pageSelectButtonPageID().innerHTML = '<span>Change</span>';
            controlData.PageID = selectedPage.Id;
        }
        else {
            jQuery(this.get_selectedPageIDLabel()).hide();
            this.get_pageSelectButtonPageID().innerHTML = '<span>Select...</span>';
            controlData.PageID = "00000000-0000-0000-0000-000000000000";
        }
    },

    /* --------------------------------- properties -------------------------------------- */

    /* PageID properties */
    get_pageSelectButtonPageID: function () {
        if (this._pageSelectButtonPageID == null) {
            this._pageSelectButtonPageID = this.findElement("pageSelectButtonPageID");
        }
        return this._pageSelectButtonPageID;
    },
    get_selectedPageIDLabel: function () {
        if (this._selectedPageIDLabel == null) {
            this._selectedPageIDLabel = this.findElement("selectedPageIDLabel");
        }
        return this._selectedPageIDLabel;
    },
    get_pageSelectorPageID: function () {
        return this._pageSelectorPageID;
    },
    set_pageSelectorPageID: function (val) {
        this._pageSelectorPageID = val;
    },
    get_selectorTagPageID: function () {
        return this._selectorTagPageID;
    },
    set_selectorTagPageID: function (value) {
        this._selectorTagPageID = value;
    }
}

SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner.registerClass('SitefinityWebApp.WidgetDesigners.SubpageSideNavigation.SubpageSideNavigationDesigner', Telerik.Sitefinity.Web.UI.ControlDesign.ControlDesignerBase);
