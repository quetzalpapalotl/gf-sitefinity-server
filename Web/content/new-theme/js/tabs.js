﻿ var windowState = 'large';
    $(document).ready(function () {

        var sw = document.body.clientWidth;
        if (sw < 735) {
            smMenu();
        }
        else {
            lgMenu();

            /* go insidelarge menu */
            setCurrent();
        }

    });

    $(window).resize(function () {
        var sw = document.body.clientWidth;
        if (sw < 735 && windowState == 'large') {
            smMenu();
        }
        if(sw > 734 && windowState == 'small') {
            lgMenu();
        }
    });

    function smMenu() {
        // add touch and click events later
        
        // change list to

        $('.tabbed-navigation').each(function () {
            var select = $('<select />');
            select.addClass('tabbed-dropdown');

            //populate select with links from the list
            $(this).find('a').each(function () {
                //go through each link and create an option in the select for each one
                var option = $('<option />');
                //populate the option with data from the links
                option.attr('value', $(this).attr('href')).html($(this).html());
                option.attr('title', $(this).text());
                //add the option to the select element
                $select.append($option);
            });

            // when an option is selected, navigate to the selected page
            select.change(function () {
                window.location = $(this).find("option:selected").val();
            });

            // target the ul and replace it with the generated select element
            $(this).replaceWith(select);
        });

        // set the current window state to small
        windowState = "small";
    }

    // convert select elements to list menu
    function lgMenu() {
        // target the select menu
        ('.tabbed-dropdown select').each(function () {
            //create an unordered list
            var ul = $("<ul />");
            ul.addClass("tabbed-navigation");

            // go through the select and cycle through each option
            $(this).find('option').each(function () {
                // for each option create a li and an achor
                var li = $('<li />');
                var anchor = $('<a />');
                //populate the anchor attributes from the option
                anchor.attr('href', $(this).attr('value')).html($(this).html());
                anchor.text($(this).attr('title'));
                //add the li and anchors to the ul
                ul.append(li);
                li.append(anchor);
            });

            // replace the select with the generated ul
            $(this).replaceWith(ul);
        });

        // set window state
        windowState = 'large';
        // set the current tab
        setCurrent();
    }

    // set the current tab
    function setCurrent() {
        var title = document.title;
        title = $.trim(title).toLowerCase();

        var listItems = $(".tabbed-navigation li");
        listItems.each(function (idx, li) {
            var anchorText = $(li).find('a:first').text();
            anchorText = $.trim(anchorText).toLowerCase();

            if (anchorText === title) {
                $(li).addClass('current-tab');

                return false;
            }
        });