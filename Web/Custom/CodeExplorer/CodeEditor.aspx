﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodeEditor.aspx.cs" Inherits="SitefinityWebApp.Custom.CodeExplorer.CodeEditor" %>
<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        body, html { padding: 0; margin: 0; }
        .lastSaved
        {
            display: none;
            z-index: 9999999; 
            background-color: #000; 
            padding: 2px 5px; 
            font-size: 11px; 
            font-weight: bold; 
            color: #fff; 
            position: absolute; 
            bottom: 0; 
            right: 0;
            font-family: Tahoma, Trebuchet MS, Arial, Times New Roman;
            opacity:0.5;
            filter:alpha(opacity=50);
        }
        .loadingPanel
        {
            text-align: center; 
            margin: -50% auto 0 auto;
            border: dotted 1px #444;
            padding: 10px;
            width: 50%;
            background-color: #ddd;
            opacity:0.7;
            filter:alpha(opacity=70);
        }
        .loadingPanel h2
        {
            display: block;
            margin: 7px;
            padding: 0;
            color: #222;
            font-family: Trebuchet MS, Arial, Times New Roman;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        </telerik:RadAjaxManager>        
        <div id="lastSaved" class="lastSaved"></div>     
        <asp:TextBox ID="txtContent" runat="server" TextMode="MultiLine" Width="100%" Height="400px">
        </asp:TextBox>
        <asp:Panel ID="pnlLoading" runat="server" CssClass="loadingPanel">
            <h2>Please wait...</h2>
        </asp:Panel>       
    </div>
    
   <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function saveToDisk(sender, eventArgs) {
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                ajaxManager.ajaxRequest(eventArgs);
            }
            function showLoadingPanel(show) {
                var loadingPanel = document.getElementById("<%= pnlLoading.ClientID %>");
                if (show == true || show == 'true')
                    loadingPanel.style.display = 'block';
                else
                    loadingPanel.style.display = 'none';
            }
        </script>
    </telerik:RadCodeBlock>

    </form>
</body>
</html>