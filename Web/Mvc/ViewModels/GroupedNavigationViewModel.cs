﻿using Babaganoush.Sitefinity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SitefinityWebApp.Mvc.ViewModels
{
	public class GroupedNavigationViewModel
	{
		public List<List<PageModel>> GroupedPages { get; set; }
	}
}
