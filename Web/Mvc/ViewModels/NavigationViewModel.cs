﻿using SitefinityWebApp.Custom.Enumerations;
using SitefinityWebApp.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SitefinityWebApp.Mvc.ViewModels
{
	public class NavigationViewModel
	{
		public ResponsiveNavigationModel NavigationItems { get; set; }
		public ControlMode ControlMode { get; set; }
	}
}
