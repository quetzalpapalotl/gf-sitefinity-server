using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Model.ContentLinks;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Mvc.Models;
using Telerik.Sitefinity.Utilities.TypeConverters;
using System.ComponentModel;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HomePageCarouselWidget", Title = "Home Page Carousel Widget", SectionName = "Global Foundries")]
    public class HomePageCarouselWidgetController : Controller
    {
        [Category("General")]
        public string Location { get; set; }

        public ActionResult Index()
        {
            var carouselItems = GetCarouselItems();

            return View("Default", carouselItems);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            View("Default", GetCarouselItems()).ExecuteResult(this.ControllerContext);
        }

        private List<HomePageCarouselWidgetModel> GetCarouselItems()
        {
            var carouselItems = new List<HomePageCarouselWidgetModel>();
            if (string.IsNullOrEmpty(Location))
                Location = "Home";

            List<DynamicContent> items = GetDynamicItems().ToList();
            items = items.Where(x => x.GetValue("Location").ToString().Contains(Location)).ToList();
            items = items.OrderBy(x => Convert.ToInt32(x.GetValue("Order"))).ToList();

            int slideNumber = 0;

            foreach (var item in items)
            {
                var carouselItem = new HomePageCarouselWidgetModel
                {
                    Title = item.GetValue("Title").ToString(),
                    MainHeading = item.GetValue("MainHeading").ToString(),
                    SubHeading = item.GetValue("SubHeading").ToString(),
                    SubSubHeading = item.GetValue("SecondSubHeading").ToString(),
                    NavigationButtonText = item.GetValue("NavigationButtonText").ToString(),
                    ImageUrl = "#",
                    SlideNumber = slideNumber,
                    CssClass = slideNumber == 0 ? "active" : "",
                    Order = Convert.ToInt32(item.GetValue("Order").ToString())
                };

                SetImageUrl(item, carouselItem);

                SetNavigationLinkUrl(item, carouselItem);
                carouselItems.Add(carouselItem);
                slideNumber++;
            }
            return carouselItems;
        }

        

        public void SetNavigationLinkUrl(DynamicContent item, HomePageCarouselWidgetModel carouselItem)
        {

            var pageNodeUrl = item.GetValue("NavigationButtonUrl").ToString();

            carouselItem.NavigationButtonUrl = pageNodeUrl;
        }

        public void SetImageUrl(DynamicContent item, HomePageCarouselWidgetModel carouselItem)
        {
            var mainImageLinks = item.GetValue("MainImage") as ContentLink[];
            if (mainImageLinks != null && mainImageLinks.Any())
            {
                var mainImageLink = mainImageLinks[0]; 
                if (mainImageLink != null)
                {
                    Guid imageId = mainImageLink.ChildItemId;
                    Image mainImage = null;
                    try
                    {
                        mainImage = LibrariesManager.GetManager().GetImage(imageId);
                    }
                    catch (Exception)
                    {
                        carouselItem.ImageUrl = "#";
                    }
                    if (mainImage != null)
                    {
                        carouselItem.ImageUrl = mainImage.Url;
                    }
                }
            }
        }

        public IQueryable<DynamicContent> GetDynamicItems()
        {
            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager();
            Type carouselitemType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Carousel.Carouselitem");

            var carouselItems = dynamicModuleManager.GetDataItems(carouselitemType).Where(cit => cit.Status == ContentLifecycleStatus.Live && cit.Visible);

            return carouselItems;
        }
    }
}