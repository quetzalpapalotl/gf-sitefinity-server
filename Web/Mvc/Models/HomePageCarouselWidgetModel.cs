namespace SitefinityWebApp.Mvc.Models
{
    public class HomePageCarouselWidgetModel
    {
        public string Title { get; set; }
        public string MainHeading { get; set; }
        public string SubHeading { get; set; }
        public string SubSubHeading { get; set; }
        public string NavigationButtonText { get; set; }
        public string NavigationButtonUrl { get; set; }
        public string ImageUrl { get; set; }
        public string CssClass { get; set; }
        public string Location { get; set; }
        public int SlideNumber { get; set; }
        public int Order { get; set; }
    }
}