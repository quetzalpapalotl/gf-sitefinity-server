﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileManager.ascx.cs" Inherits="SitefinityWebApp.Custom.CodeExplorer.FileManager" %>

<!-- SHOULD ADD ON PAGE LEVEL OR IN CODE TO ENSURE NO DUPLICATES SCRIPT MANAGER ON THE PAGE -->
<telerik:RadScriptManager ID="RadScriptManager1" Runat="server"> 
</telerik:RadScriptManager> 

<h1 id="sfToMainContent" class="sfBreadCrumb">Code Explorer</h1>

<asp:Panel ID="pnlFileManager" runat="server">    
    <div style="margin: 10px 0 0 10px;">
        <asp:Literal ID="ltrMessages" runat="server"></asp:Literal>
        <telerik:RadFileExplorer ID="RadFileExplorer1"  runat="server"
            OnClientFileOpen="RadFileExplorer1_ClientFileOpen"
            Skin="Sitefinity">
        </telerik:RadFileExplorer>
    </div>
</asp:Panel>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        var newWindowOptions = 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=600,height=500,top=100,left=200';
        var newWindowTitle = 'CodeEditor';

        function getVirtualPath(path) {
            return "~/" + path.substring(baseUrl.length);
        }

        function getEditorPath(path) {
            return codeEditorUrl + '?file=' + escape(getVirtualPath(path));
        }

        function OnGridContextItemClicked(oGridMenu, args) {
            var oExplorer = $find("<%= RadFileExplorer1.ClientID %>");
            var selectedItem = oExplorer.get_selectedItem();
            if (selectedItem) {
                var itemPath = selectedItem.get_path();
                var menuItemText = args.get_item().get_text();
                switch (menuItemText) {
                    case "Download":
                        window.open(itemPath);
                        break;
                    case "Edit":
                        window.open(getEditorPath(itemPath), newWindowTitle, newWindowOptions);
                        break;
                }
            }
        }

        //TO EDIT FILE CONTENTS
        function RadFileExplorer1_ClientFileOpen(oExplorer, args) {
            var item = args.get_item();
            var fileExtension = item.get_extension();
            if (fileExtension == "htm" ||
                fileExtension == "html" ||
                fileExtension == "css" ||
                fileExtension == "js" ||
                fileExtension == "txt" ||
                fileExtension == "skin" ||
                fileExtension == "master" ||
                fileExtension == "php" ||
                fileExtension == "cs" ||
                fileExtension == "vb" ||
                fileExtension == "ascx" ||
                fileExtension == "aspx" ||
                fileExtension == "asp" ||
                fileExtension == "sql" ||
                fileExtension == "config" ||
                fileExtension == "xml" ||
                fileExtension == "pl" ||
                fileExtension == "java" ||
                fileExtension == "cf" ||
                fileExtension == "rb" ||
                fileExtension == "log") {
                // Do not open a default window
                args.set_cancel(true);

                // Open file within a new window editor instead
                var editorPath = getEditorPath(item.get_path());
                window.open(editorPath, newWindowTitle, newWindowOptions);
            }
        }
    </script>
</telerik:RadCodeBlock>