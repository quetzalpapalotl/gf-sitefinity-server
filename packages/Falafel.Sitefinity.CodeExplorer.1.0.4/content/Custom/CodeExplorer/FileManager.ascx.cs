﻿using System;
using System.Web.UI;
using Telerik.Web.UI;

namespace SitefinityWebApp.Custom.CodeExplorer
{
    public partial class FileManager : System.Web.UI.UserControl
    {
        public const string DEFAULT_ALLOWED_PATHS = "~/App_Data/Sitefinity/WebsiteTemplates";
        public const string DEFAULT_ALLOWED_EXT = ".htm,.html,.css,.js,.txt,.skin,.master,.xml";
        public const int DEFAULT_MAX_FILE_UPLOAD_SIZE = 104857600;
        public const string CODE_EDITOR_PATH = "~/Custom/CodeExplorer/CodeEditor.aspx";
        public const string EDIT_AREA_PATH = "~/Custom/CodeExplorer/Scripts/edit_area/edit_area_full.js";
        private const string JS_TAG = @"
            <script type=""text/javascript""> 
                //<![CDATA[
                {0}        
                //]]>
            </script>";

        public string AllowedPaths
        {
            get;
            set;
        }

        public string AllowedExt
        {
            get;
            set;
        }

        public int MaxUploadFileSize
        {
            get;
            set;
        }

        public FileManager()
        {
            this.AllowedPaths = DEFAULT_ALLOWED_PATHS;
            this.AllowedExt = DEFAULT_ALLOWED_EXT;
            this.MaxUploadFileSize = DEFAULT_MAX_FILE_UPLOAD_SIZE;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //OUTPUT FILE MANAGER SETTINGS
            ltrMessages.Text = String.Format(@"
                <strong>Allowed Paths:</strong> {0} <br />
                <strong>Allowed Extensions:</strong> {1} <br /> <br />",
                this.AllowedPaths, this.AllowedExt);

            this.InitializeFileExplorer();
        }

        protected void InitializeFileExplorer()
        {
            //ADD DOWNLOAD TO CONTEXT MENU
            RadMenuItem item = new RadMenuItem("Download");
            item.PostBack = false;
            item.Value = "Download";
            //ADD DOWNLOAD TO CONTEXT MENU
            RadFileExplorer1.GridContextMenu.Items.Add(item);
            item = new RadMenuItem("Edit");
            item.PostBack = false;
            item.Value = "Edit";
            RadFileExplorer1.GridContextMenu.Items.Add(item);
            //ENABLE CLIENT-SIDE RETRIEVAL OF FILE PATHS WHEN SELECTED
            RadFileExplorer1.GridContextMenu.OnClientItemClicked = "OnGridContextItemClicked";
            //WORKING DIRECTORIES
            string[] allowedPaths = this.AllowedPaths.Split(new char[] { ',' });
            //AUTOMATICALLY USE FIRST PATH IN LIST FOR INITIAL PATH
            RadFileExplorer1.InitialPath = Page.ResolveUrl(allowedPaths[0]);
            RadFileExplorer1.Configuration.ViewPaths = allowedPaths;
            RadFileExplorer1.Configuration.UploadPaths = allowedPaths;
            RadFileExplorer1.Configuration.DeletePaths = allowedPaths;
            //SET FILE UPLOAD SIZE
            RadFileExplorer1.Configuration.MaxUploadFileSize = this.MaxUploadFileSize;

            //USED TO RESOLVE URL IN JAVASCRIPT
            this.Page.Header.Controls.Add(new LiteralControl(String.Format(JS_TAG, String.Format(
                "var baseUrl='{0}';\n var codeEditorUrl='{1}';\n", Request.ApplicationPath, ResolveUrl(CODE_EDITOR_PATH)))));
        }
    }
}